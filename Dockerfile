# Stage 1: Build

FROM node:16-alpine AS build

RUN mkdir -p /app
WORKDIR /app

COPY package.json package-lock.json /app/
RUN npm ci
COPY . /app/

RUN npm run build


# Stage 2

FROM nginx:1.23.3-alpine

COPY --from=build /app/dist/host-fe/ /usr/share/nginx/html/host/